const fs = require('fs');
const file = require('./file-promise.js');
const opts = { encoding : 'utf-8'};

function getFileObject(name, content){
	return { 
		name,
		content
	}
}
module.exports = function(path){
	return new Promise((done, fail) => {
					fs.readdir(path, (err, names) => {
						if (err){
							fail(err);
						}
						//console.log(names);
						Promise.all(names.map(name => file.read(path.concat(name))))
						.then(data => done(data.map((content,ind) => getFileObject(names[ind], content))))
						.catch(err => fail(err));
					});
				});
}