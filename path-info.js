const fs = require('fs');
const opts = {'encoding' : 'utf-8'};

module.exports = function(path, callback){
	
	let err = null;
	let info = { path : path };	
	
	fs.stat(path, function(err, stats){
		if (err){
			return err;			
		}
		if (stats.isDirectory()){
			info.type = 'directory';
			fs.readdir(path, function(err, files){
				info.childs = files;
				callback(err,info);
			});
		}else if (stats.isFile()){
			info.type = 'file';
			fs.readFile(path, opts, (err, data) => {
				info.content = data;
				callback(err,info);
			});		
		}else{
			callback(new Error("cannot read path"), null);
		}
	});	
};