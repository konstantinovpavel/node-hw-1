const fs = require('fs')
const opts = {encoding : 'utf-8'};

const read = function(filePath){
	return new Promise((done, fail) => {
		fs.readFile(filePath, opts, (err, data) => {
			if (err){
				fail(err);
			}else{
				done(data);
			}
		});
	});
}

const write = function(filePath, data){
	return new Promise((done, fail) => {
		fs.writeFile(filePath, data, err => {
			if (err){
				fail(err);
			}else{
				done(filePath);
			}
		});
	});
	
}

module.exports = {
	read 	: read,
	write 	: write
}